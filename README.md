Author: Refael Yehuda, refaely@uoregon.edu

Class: CIS 322

Date: 11/06/18

Description: In this project, I implemented the RUSA ACP controle time calculator with flask and ajax. 

The algorithm for time calculation was taken from the following website: https://rusa.org/pages/acp-brevet-control-times-calculator

Based on the table:

[Control location, Minimum Speed,  Maximum Speed]

[0 - 200, 15, 34]

[200 - 400, 15 , 32]

[400 - 600, 15, 30]

[600 - 1000, 11.428, 28]

[1000 - 1300, 13.333, 26]


If we are given a 200 km bevet with controls at 60 km, 120 km, 175 km, and 200 km, we can calculate the opening time of each control point by dividing the distance with the maximum speed. 

In this Case:

60/34 = 1.76

We now need to convert it to hours bu subtracting the number before the decimal and multiplying by 60.

So:

0.76 * 60 = 0.45.6 which is 1 hour and about 56 minutes. 

Thus, if we started at 12:00  we the the contol point at 60 km will open at 13:46 (12:00 + 1:46 = 1:46).

To find the closing time of each contol point, we need to divide the distance of the control point with the minium speed.

In this case:

60/15 = 4

So this contol point will close 4 hours after the starting time (12:00 + 4:00 = 4:00).

In addition, the first control point (0 km) will always close an hour after it opened. 




