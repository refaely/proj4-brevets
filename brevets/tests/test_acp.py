from acp_times import open_time, close_time
import arrow
import nose

#testing the open_time function
def test_open_time():
    o_time = arrow.utcnow()
    assert open_time(200, 400, o_time) == o_time.shift(hours=(200/34)).isoformat()

#testing the close_time function
def test_close_time():
    c_time = arrow.utcnow()
    assert close_time(100, 800, c_time) == c_time.shift(hours=100/15).isoformat()

#testing when control time is 0
def test_first_control_point():
    c_time = arrow.utcnow()
    assert close_time(0, 400, c_time) == c_time.shift(hours=1).isoformat()

#testing endtime of the 200 km brevet
def test_200_endtime():
    c_time = arrow.utcnow()
    with_minutes = close_time.shift(minutes=10)
    assert close_time(200, 200, c_time) == with_minutes.shift(hours=200/15).isoformat()

#testing endtime of the 400 km brevet
def test_400_endtime():
    c_time = arrow.utcnow()
    with_minutes = closetime.shift(minutes=20)
    assert close_time(400, 400, c_time) == with_minutes.shift(hours=400/15).isoformat()
