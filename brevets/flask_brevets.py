"""
    Replacement for RUSA ACP brevet time calculator
    (see https://rusa.org/octime_acp.html)
    
    """

import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
        Calculates open/close times from miles, using rules
        described at https://rusa.org/octime_alg.html.
        Expects one URL-encoded argument, the number of miles.
        """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    #open_time = acp_times.open_time(km, 200, arrow.now().isoformat)
    
    #Creating a timezone variable
    timezone = request.args.get('tz', type=str)
    
    #Getting nomial distance of the brevet
    b_distance = request.args.get('brev_dist', type=int)
    
    #creates beginning date time arrow object from passed arguments in ajax script
    begintime = arrow.get(request.args.get('bd', type=str) + " " + request.args.get('bt', type=str), 'YYYY-MM-DD HH:mm').replace(tzinfo=timezone).isoformat()
    
    #begintime = arrow.get(request.args.get('bd', type=str) + " " + request.args.get('bt', type=str), 'YYYY-MM-DD HH:mm').isoformat()
    
    open_time = acp_times.open_time(km, b_distance, begintime)
    #return false if control distance is greater than 1.20 * brevet_dist before calculating closetime
    if not open_time:
        return flask.jsonify(result=False)
    
    #close_time = acp_times.close_time(km, 200, arrow.now().isoformat)
    
    #continue with calculation of times if opentime was given valid control distance
    close_time = acp_times.close_time(km, b_distance, begintime)
    
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")

#secret key
app.secret_key = '\x19\xe8s*VDD\x8f\xd47\xaa\xbe\xf0\x15(\xa8ZN\xb7\xa0\x18\xfb/\xbd'
